$('ul').on('click','li',function(){
	$(this).closest('ul').find('li').removeClass('sel');
	$(this).addClass('sel');
	$('[name="'+$(this).closest('ul').attr('class')+'"]').val($(this).attr('name'));
	if ($(this).closest('ul').hasClass('tariff_id')){
		console.log($(this).find('.price').text())
		$('#form [name="sum"]').val($(this).find('.price').text());
	}
});


$('#submit').on('click',function(e){
	e.preventDefault();
	var msg = $('#form').serialize();
	$.ajax({
		type: "POST",
		url: "/ajax/setOrder",
		data: msg, 
		success: function(data){
			$('#form [name="customerNumber"]').val(data);
			$('#form').submit();
		}
	});
});