<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index_page.index');
});
Route::post('/', function () {
    return view('welcome');
});

Route::get('/payment', ['as' => 'page_payment', 'uses' => 'PaymentsController@paymentPage']);

Route::get('/tariff', function () {
    return view('pages.tariff');
});

Route::post('/crm/vk', ['as' => 'vk', 'uses' => 'IndexController@index']);
Route::get('/api/vk', ['as' => 'vk_api', 'uses' => 'VkApiController@index']);

//Ajax
Route::post('/ajax/setOrder', ['as' => 'ajax_set_order', 'uses' => 'AjaxController@setOrder']);

//платежи Яндекс.Касса
Route::get('/payments/check', ['as' => 'payment_check', 'uses' => 'PaymentsController@check']);
Route::get('/payments/aviso', ['as' => 'payment_aviso', 'uses' => 'PaymentsController@aviso']);
Route::get('/payments/success', ['as' => 'payment_success', 'uses' => 'PaymentsController@success']);
Route::get('/payments/fail', ['as' => 'payment_fail', 'uses' => 'PaymentsController@fail']);
Route::post('/payments/check', ['as' => 'payment_check', 'uses' => 'PaymentsController@check']);
Route::post('/payments/aviso', ['as' => 'payment_aviso', 'uses' => 'PaymentsController@aviso']);

Route::get('/payments-test/check', ['as' => 'payment_check', 'uses' => 'PaymentsController@check']);
Route::get('/payments-test/aviso', ['as' => 'payment_aviso', 'uses' => 'PaymentsController@aviso']);
Route::get('/payments-test/success', ['as' => 'payment_success', 'uses' => 'PaymentsController@success']);
Route::get('/payments-test/fail', ['as' => 'payment_fail', 'uses' => 'PaymentsController@fail']);
Route::post('/payments-test/check', ['as' => 'payment_check', 'uses' => 'PaymentsController@check']);
Route::post('/payments-test/aviso', ['as' => 'payment_aviso', 'uses' => 'PaymentsController@aviso']);

//тестирование
Route::get('/test', ['as' => 'test_page', 'uses' => 'TestController@index']);
Route::get('/test2', ['as' => 'test_page', 'uses' => 'TestController@test2']);