<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\VkAuthController;

use App\Models\Client;
use App\Models\Comment;

class IndexController extends Controller
{
    
    private $oCatalogController;
    private $oOrderController;
    private $oVkAuthController;
    private $oSettingsController;
    
    public function __construct() {
        
        $this->oCatalogController = new CatalogController();
        $this->oOrderController = new OrdersController();
        $this->oSettingsController = new SettingsController();
        $this->oVkAuthController = new VkAuthController();
    }
    
    public function index(Request $request, Client $oClient)
    {
        $strMethod = $request->input('method');
        $strController = $request->input('controller');
        
        //на прямую пускаю только для проверки пользователя, в других случаях через проверку авторизации
        if ($strController == 'VkAuthController')
        {
            $this->oVkAuthController->$strMethod($request->input());
            return;
        }
        else
        {
            //проверка авторизованного пользователя
            if (!$this->oVkAuthController->userOnline($request->input()))
            {
                //если пользователя такого нет а базе, то высылаем окно авторизации
                $aResult = ['result' => -1];
                print json_encode($aResult);
                return;
                exit();
            }
        }
        //--
        
        switch ($strController)
        {
            case 'ClientsController':
                ClientsController::$strMethod($request->input(), $oClient);
                break;
            
            case 'CatalogController':
                $this->oCatalogController->$strMethod($request->input());
                break;
            
            case 'OrdersController':
                $this->oOrderController->$strMethod($request->input());
                break;
            
            case 'VkAuthController':
                $this->oVkAuthController->$strMethod($request->input());
                break;
            
            case 'SettingsController':
                $this->oSettingsController->$strMethod($request->input());
                break;
        }
        
    }
    
    
}
