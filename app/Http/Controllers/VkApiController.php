<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use Carbon\Carbon;

use App\Models\Catalog;
use App\Models\UserToken;

use App\Http\Controllers\HtmlParserController;

class VkApiController extends Controller
{
    private $nIdApp;
    private $strSecretKeyApp;
    private $strUrlRedirect;
    private $strScopeMarketCode;
    private $strScopeMarketName;
    
    public function __construct() 
    {
        $this->nIdApp = env('nIdApp');
        $this->strSecretKeyApp = env('strSecretKeyApp');
        $this->strUrlRedirect = env('strUrlRedirect');
        $this->strScopeMarketCode = env('strScopeMarketCode');
        $this->strScopeMarketName = env('strScopeMarketName');
    }

    public function index(Request $request)
    {
        $strCode  = $request->input('code');
//        $strState = $request->input('state'); //author_id
//        
//        $strAccessToken = file_get_contents('https://oauth.vk.com/access_token?client_id='.$this->nIdApp.'&client_secret='.$this->strSecretKeyApp.
//                '&redirect_uri='.$this->strUrlRedirect.'&code='.$strCode);
//        $jsonAccessToken = json_decode($strAccessToken);
//        $strAccessToken = $jsonAccessToken->access_token;
//        
//        if (!empty($strAccessToken))
//        {
//            //тут читаю данные по клиенту где нужен access_token, если чтение прошло успешно, значит id пользователя vk не подделано
//            
//            //-----------
//            
//            $oUserToken = UserToken::where('author_id', '=', $strState)
//                    ->first();
//            
//            if (empty($oUserToken->id))
//            {
//                $aData = [
//                    'author_id' => $strState,
//                    'access_token' => $strAccessToken,
//                    'sreda_key' => md5($strAccessToken),
//                    'social_network' => 'vk',
//                ];
//                
//                UserToken::create($aData);
//            }
//            else
//            {
//                $aData = [
//                    'access_token' => $strAccessToken,
//                    'sreda_key' => md5($strAccessToken),
//                ];
//                
//                UserToken::where('author_id', '=', $strState)
//                        ->update($aData);
//            }
//            
//            //setcookie('sreda_key', md5($strAccessToken), time() + (43800 * 60) * 6, '/', 'vk.com');  /* 43800 * 60 - 1 месяц */
//            
//            $aResult = ['result' => 1, 'data' => ['sreda_key' => md5($strAccessToken)] ];
//        }
//        else $aResult = ['result' => 0];
        
        $strState = $request->input('state');
        
        $aFiledValue = explode('||', $strState);
        
        foreach ($aFiledValue as $key => $strVal)
        {
            $aData = explode('::', $strVal);
            
            if ($aData[0] == 'author_id') $aPost['author_id'] = $aData[1];
            if ($aData[0] == 'shop_url') $aPost['options']['shop_url'] = $aData[1];
            if ($aData[0] == 'name_category') $aPost['options']['name_category'] = $aData[1];
        }
        
        if ($this->importFromVkShop($aPost, $strCode))
        {
            $aResult = 'Данные успешно импортированы, через несколько секунд вы будите перенаправлены на свою страницу'
                    . '<script>setTimeout (function(){document.location.href="https://vk.com"},5000)</script>'; //['result' => 1];
        }
        else $aResult = 'Ошибка импорта'; //['result' => 0];
        
        //print json_encode($aResult);
        print $aResult;
        
        return;
    }
    
    public function getUrl($aPost)
    {
        return 'https://oauth.vk.com/authorize?client_id='.$this->nIdApp.'&display=page&redirect_uri='.$this->strUrlRedirect.'&scope='.$this->strScopeMarketName.
                '&response_type=code&v=5.60&state=author_id::'.$aPost['author_id'].'||shop_url::'.$aPost['options']['shop_url'].'||name_category::'.$aPost['options']['name_category'];
        
//        return 'https://oauth.vk.com/authorize?client_id='.$this->nIdApp.'&display=page&redirect_uri='.$this->strUrlRedirect.'&scope='.$this->strScopeMarketName.
//                '&response_type=code&v=5.60&state='.$aPost['author_id'];
    }

    public function importFromVkShop($aPost, $strCode)
    {
        if (!empty($aPost['author_id']) && !empty($aPost['options']['shop_url']))
        {
            $strPage = file_get_contents($aPost['options']['shop_url']);
            $strHtml = HtmlParserController::from_string($strPage);
            
            $aString = explode('vk.com/', $strPage);
            $aMarket = explode('-', $aString[1]);
            
            if (!empty($aMarket[1]) && intval(preg_replace("/[^0-9]/", '', $aMarket[1])) > 0 )
            {
                $strMarketId = '-'.preg_replace("/[^0-9]/", '', $aMarket[1]);
            }
            
            //если указана ссылка не на товары а просто на группу, то нахожу ссылку на товары
            if (empty($strHtml->find('#market_list div .market_row_name a')->text))
            {
                foreach($strHtml->find('.pm_item') as $element) 
                {
                    if (strripos($element->href, 'market') > 0) 
                    {
                        $strPage = file_get_contents('https://vk.com'.$element->href);
                        $strHtml = HtmlParserController::from_string($strPage);
                        
                        $aString = explode('-', $element->href);
                        $aLink = explode('?',  $aString[1]); //"-130806445?gml=1"
                        $strMarketId = '-'.preg_replace("/[^0-9]/", '', $aLink[0]);
                        
                        break;
                    }
                }
            }
            
            $aCatalog = [
                'author_id' => $aPost['author_id'],
                'name' => htmlspecialchars( urldecode(iconv ( 'windows-1251' , 'utf-8' , $aPost['options']['name_category'] )) ),
                'it_category' => 1,
                'category_id' => 0,
            ];

            Catalog::create($aCatalog);

            $nLastId = Catalog::all()->last()->id;
            
            $strAccessToken = @file_get_contents('https://oauth.vk.com/access_token?client_id='.$this->nIdApp.'&client_secret='.$this->strSecretKeyApp.'&redirect_uri='.$this->strUrlRedirect.'&code='.$strCode);
            //$strAccessToken = $this->get_curl('https://oauth.vk.com/access_token?client_id='.$this->nIdApp.'&client_secret='.$this->strSecretKeyApp.'&redirect_uri='.$this->strUrlRedirect.'&code='.$strCode);
            //if (!empty($strAccessToken))
            {
                $jsonAccessToken = json_decode($strAccessToken);
                $strAccessToken = $jsonAccessToken->access_token;
            }
//            else
//            {
//                print 'Ошибка VK, 401 <br>';
//                return false;
//            }
            
            //парсинг самого магазина
            $strMarketData = file_get_contents('https://api.vk.com/method/market.get?owner_id='.$strMarketId.'&count=200&offset=0&access_token='.$strAccessToken.'&v=5.52&scope='.$this->strScopeMarketCode);
            $jsonMarketData = json_decode($strMarketData);
            
            $nCountItems = $jsonMarketData->response->count;
            
            $k = 0;
            foreach ($jsonMarketData->response->items as $key => $oVal)
            {
                $aResult[] = [
                    'name' => $oVal->title,
                    'description' => htmlspecialchars($oVal->description),
                    'price' => intval(preg_replace("/[^0-9]/", '', $oVal->price->text)),
                    'image' => $oVal->thumb_photo,

                    'author_id' => $aPost['author_id'],
                    'it_category' => 0,
                    'category_id' => $nLastId,
                ];
                
                $k++;
            }
            
            //если товаров больше чем максимально возможное 200
            while ($k < $nCountItems)
            {
                $strMarketData = file_get_contents('https://api.vk.com/method/market.get?owner_id='.$strMarketId.'&count=200&offset='.$k.'&access_token='.$strAccessToken.'&v=5.52&scope='.$this->strScopeMarketCode);
                $jsonMarketData = json_decode($strMarketData);
                
                foreach ($jsonMarketData->response->items as $key => $oVal)
                {
                    $aResult[] = [
                        'name' => htmlspecialchars($oVal->title),
                        'description' => htmlspecialchars($oVal->description),
                        'price' => intval(preg_replace("/[^0-9]/", '', $oVal->price->text)),
                        'image' => $oVal->thumb_photo,

                        'author_id' => $aPost['author_id'],
                        'it_category' => 0,
                        'category_id' => $nLastId,
                    ];

                    $k++;
                }
                
                usleep(400000); //0.4 секунды
            }
            
            DB::table('catalog')->insert($aResult);
            
            return true;
        }
        else return false;
        
    }
    
    function get_curl($url) 
    {
        if(function_exists('curl_init')) 
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $output = curl_exec($ch);
            echo curl_error($ch);
            curl_close($ch);
            return $output;
        } 
        else 
        {
            return file_get_contents($url);
        }
    }
    
    public function importFromVkShop2($aPost)
    {
        if (!empty($aPost['author_id']) && !empty($aPost['options']['shop_url']))
        {
            $oData = UserToken::where('author_id', '=', $aPost['author_id'])
                    ->first();
            
            if (empty($oData->id)) return false;
            
            $strAccessToken = $oData->access_token;
            
            $strPage = file_get_contents($aPost['options']['shop_url']);
            $strHtml = HtmlParserController::from_string($strPage);
            
            $aString = explode('vk.com/', $strPage);
            $aMarket = explode('-', $aString[1]);
            
            if (!empty($aMarket[1]) && intval(preg_replace("/[^0-9]/", '', $aMarket[1])) > 0 )
            {
                $strMarketId = '-'.preg_replace("/[^0-9]/", '', $aMarket[1]);
            }
            
            //если указана ссылка не на товары а просто на группу, то нахожу ссылку на товары
            if (empty($strHtml->find('#market_list div .market_row_name a')->text))
            {
                foreach($strHtml->find('.pm_item') as $element) 
                {
                    if (strripos($element->href, 'market') > 0) 
                    {
                        $strPage = file_get_contents('https://vk.com'.$element->href);
                        $strHtml = HtmlParserController::from_string($strPage);
                        
                        $aString = explode('-', $element->href);
                        $aLink = explode('?',  $aString[1]); //"-130806445?gml=1"
                        $strMarketId = '-'.preg_replace("/[^0-9]/", '', $aLink[0]);
                        
                        break;
                    }
                }
            }
            
            $aCatalog = [
                'author_id' => $aPost['author_id'],
                'name' => htmlspecialchars( urldecode(iconv ( 'windows-1251' , 'utf-8' , $aPost['options']['name_category'] )) ),
                'it_category' => 1,
                'category_id' => 0,
            ];

            Catalog::create($aCatalog);

            $nLastId = Catalog::all()->last()->id;
            
            //парсинг самого магазина
            $strMarketData = file_get_contents('https://api.vk.com/method/market.get?owner_id='.$strMarketId.'&count=200&offset=0&access_token='.$strAccessToken.'&v=5.52&scope='.$this->strScopeMarketCode);
            $jsonMarketData = json_decode($strMarketData);
            
            $nCountItems = $jsonMarketData->response->count;
            
            $k = 0;
            foreach ($jsonMarketData->response->items as $key => $oVal)
            {
                $aResult[] = [
                    'name' => htmlspecialchars($oVal->title),
                    'description' => htmlspecialchars($oVal->description),
                    'price' => intval(preg_replace("/[^0-9]/", '', $oVal->price->text)),
                    'image' => $oVal->thumb_photo,

                    'author_id' => $aPost['author_id'],
                    'it_category' => 0,
                    'category_id' => $nLastId,
                ];
                
                $k++;
            }
            
            //если товаров больше чем максимально возможное 200
            while ($k < $nCountItems)
            {
                $strMarketData = file_get_contents('https://api.vk.com/method/market.get?owner_id='.$strMarketId.'&count=200&offset='.$k.'&access_token='.$strAccessToken.'&v=5.52&scope='.$this->strScopeMarketCode);
                $jsonMarketData = json_decode($strMarketData);
                
                foreach ($jsonMarketData->response->items as $key => $oVal)
                {
                    $aResult[] = [
                        'name' => htmlspecialchars($oVal->title),
                        'description' => htmlspecialchars($oVal->description),
                        'price' => intval(preg_replace("/[^0-9]/", '', $oVal->price->text)),
                        'image' => $oVal->thumb_photo,

                        'author_id' => $aPost['author_id'],
                        'it_category' => 0,
                        'category_id' => $nLastId,
                    ];

                    $k++;
                }
                
                usleep(400000); //0.4 секунды
            }
            
            DB::table('catalog')->insert($aResult);
            
            return true;
        }
        else return false;
        
    }
}
