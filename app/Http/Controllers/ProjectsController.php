<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use Carbon\Carbon;

use App\Models\Project;
use App\Models\ProjectExecuter;

class ProjectsController extends Controller
{
    
    public function getProjectsList($aPost)
    {
        $aProjects = Project::where('author_id', '=', $aPost['author_id'])
                ->where('it_group', '=', 1)
                ->orderBy('id', 'desc')
                ->get()
                ->toArray();
        
        if (!empty($aProjects)) $aResult = ['result' => 1, 'data' => $aProjects];
        else $aResult = ['result' => 1, 'data' => []];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getChecklist($aPost)
    {
        $aCheckList = Project::with('project_executers')
                ->where('author_id', '=', $aPost['author_id'])
                ->where('parent_id', '=', $aPost['options']['project_id'])
                ->where('it_group', '=', 0)
                ->where('it_checklist', '=', 1)
                ->orderBy('id', 'asc')
                ->get()
                ->toArray();
        
        if (!empty($aCheckList)) $aResult = ['result' => 1, 'data' => $aCheckList];
        else $aResult = ['result' => 1, 'data' => []];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getExecuterList($aPost)
    {
        $aProjectExecuter = ProjectExecuter::where('author_id', '=', $aPost['author_id'])
                ->where('parent_id', '=', $aPost['options']['parent_id'])
                ->get()
                ->toArray();
        
        if (!empty($aProjectExecuter)) $aResult = ['result' => 1, 'data' => $aProjectExecuter];
        else $aResult = ['result' => 1, 'data' => []];
        
        print json_encode($aResult);
        
        return;
    }
    
    
}
