<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use Carbon\Carbon;

use App\Models\Client;
use App\Models\Comment;
use App\Models\ClientField;
use App\Models\ClientGroup;
use App\Models\ClientUserGroup;
use App\Models\Payment;
use App\Models\UserToken;

use App\Http\Controllers\SiteController;

class ClientsController extends Controller
{
    private static $nClientId;
    
    public static function addClient($aPost, Client $oClient)
    {
        //изначально проверяю на лимиты, только потом пропускаю
//        $bLimit = SiteController::bCheckPayLimit($aPost, 'clients', 50);
//        
//        if (!$bLimit)
//        {
//            print json_encode(['result' => 0, 'message' => 'Превышен лимит, либо период оплаты исчерпан']);
//            return;
//        }
        //-------
        
        $oData = Client::where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->first();
        
        if (empty($oData))
        {
            $aData = [
                'author_id' => $aPost['author_id'],
                'client_id' => $aPost['options']['client_id'],
                'parent_id' => empty($aPost['options']['parent_id']) ? 0 : $aPost['options']['parent_id'],
                'status_id' => empty($aPost['options']['status_id']) ? 0 : $aPost['options']['status_id'],
                'type_id' => empty($aPost['options']['type_id']) ? 0 : $aPost['options']['type_id'],
                'group_id' => empty($aPost['options']['group_id']) ? 0 : $aPost['options']['group_id'],
                'name' => htmlspecialchars($aPost['options']['name']),
                'city' => empty($aPost['options']['city']) ? '' : htmlspecialchars($aPost['options']['city']),
                'phone' => empty($aPost['options']['phone']) ? '' : htmlspecialchars($aPost['options']['phone']),
                'image' => empty($aPost['options']['image']) ? '' : $aPost['options']['image'],
                'work_phone' => empty($aPost['options']['work_phone']) ? '' : htmlspecialchars($aPost['options']['work_phone']),
                'date_of_birth' => empty($aPost['options']['date_of_birth']) ? '' : htmlspecialchars($aPost['options']['date_of_birth']),
                'place_of_work' => empty($aPost['options']['place_of_work']) ? '' : htmlspecialchars($aPost['options']['place_of_work']),
                'site' => empty($aPost['options']['site']) ? '' : htmlspecialchars($aPost['options']['site']),
                'email' => empty($aPost['options']['email']) ? '' : htmlspecialchars($aPost['options']['email']),
                'skype' => empty($aPost['options']['skype']) ? '' : htmlspecialchars($aPost['options']['skype']),
            ];
            
            $oClient->create($aData);
            
            $aData = null;
            
            if (!empty($aPost['options']['contact_fields']))
            {
                foreach ($aPost['options']['contact_fields'] as $key => $aVal)
                {
                    $aData[] = [
                        'author_id' => $aPost['author_id'],
                        'client_id' => $oClient->all()->last()->id,
                        'field_group' => empty($aVal['field_group']) ? '' : $aVal['field_group'],
                        'field_name' => empty($aVal['field_name']) ? '' : htmlspecialchars($aVal['field_name']),
                        'field_value' => empty($aVal['field_value']) ? '' : htmlspecialchars($aVal['field_value']),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }

                DB::table('clients_fields')->insert($aData);
            }
            
            print json_encode(['result' => 1, 'data' => ['id' => $oClient->all()->last()->id]]);
        }
        else print json_encode(['result' => 0, 'message' => 'Данный клиент уже занесён в базу']);
        
        return;
    }
    
    public static function getClientsList($aPost, Client $oClient)
    {
        $nAuthorId = $aPost['author_id'];
        
        //формирую условие поиска
        $aQuery[] = ' 1 = ? ';
        $aQueryValues[] = 1;
        
        if (!empty($aPost['options']['name']))
        {
            $aQuery[] = ' name LIKE ? ';
            $aQueryValues[] = '%'.$aPost['options']['name'].'%';
        }
        
        if (!empty($aPost['options']['phone']))
        {
            $aQuery[] = ' phone LIKE ? ';
            $aQueryValues[] = '%'.$aPost['options']['phone'].'%';
        }
        
        if (!empty($aPost['options']['city']))
        {
            $aQuery[] = ' citye LIKE ? ';
            $aQueryValues[] = '%'.$aPost['options']['city'].'%';
        }
        
        $strQuery = implode(' AND ', $aQuery);
        //-----------------------
        
        //упорядочивание
        if (empty($aPost['options']['order_method']) && empty($aPost['options']['order_field']))
        {
            $strField = 'id';
            $strOrder = 'DESC';
        }
        else
        {
            $strField = $aPost['options']['order_field'];
            $strOrder = $aPost['options']['order_method'];
        }
        
        //группа клиента
        if (!isset($aPost['options']['group_id']) || $aPost['options']['group_id'] == 0)
        {
            $strGroupCondition = '<>';
            $strGroupValue = -1;
            
            $strQueryGroup = ' group_id <> ? ';
            $aQueryGroupValue[] = -1;
        }
        else
        {
            $strGroupCondition = '=';
            $strGroupValue = $aPost['options']['group_id'];
            
            $strQueryGroup = '(group_id = ? OR group_id IN (SELECT parent_id FROM clients_user_groups WHERE author_id = ? AND parent_id = ? ))';
            $aQueryGroupValue = [$aPost['options']['group_id'], $nAuthorId, $aPost['options']['group_id']];
        }
        
        if (!empty($aPost['options']['type_id']) && intval($aPost['options']['type_id']) > 0)
        {
            $strTypeIdCondition = '=';
            $nTypeIdValue = $aPost['options']['type_id'];
        }
        else
        {
            $strTypeIdCondition = '<>';
            $nTypeIdValue = -1;
        }
        
        $aClient = Client::where('author_id', '=', $nAuthorId)
                //->where('group_id', $strGroupCondition, $strGroupValue)
                ->where('type_id', $strTypeIdCondition, $nTypeIdValue)
                ->whereRaw($strQueryGroup, $aQueryGroupValue)
                ->whereRaw($strQuery, $aQueryValues)
                ->orderBy($strField, $strOrder)
                ->get()
                ->toArray();
        
        if (!empty($aClient)) 
        {
            $aResult = ['result' => 1, 'data' => $aClient];
        }
        else 
        {
            //$aResult = ['result' => 0];
            //Верстальщик просит при пустой базе передавать true
            $aResult = ['result' => 1, 'data' => []];
        }
        
        print json_encode($aResult);
        
        return;
    }
    
    public static function getClientData($aPost, Client $oClient)
    {
        self::$nClientId = $aPost['options']['id'];
        
        $aClient = Client::with(['contact_fields' => function ($query) {
                    $query->where('client_id', '=', self::$nClientId);
                }])
                ->where('author_id', '=', $aPost['author_id'])
                ->where('id', '=', $aPost['options']['id'])
                ->first()
                ->toArray();
        
        if (!empty($aClient)) $aResult = ['result' => 1, 'data' => $aClient];
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public static function removeClient($aPost)
    {
        DB::table('clients')->where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->delete();
        ClientField::where('author_id', '=', $aPost['author_id'])->delete();
        
        print json_encode(['result' => 1]);
        
        return;
    }
    
    public static function updateClient($aPost)
    {
        foreach ($aPost['options'] as $key => $strVal)
        {
            if ($key != 'img' && $key != 'id' && $key != 'contact_fields')
            {
                $aData[$key] = htmlspecialchars($strVal);
            }
        }
        $aData['updated_at'] = Carbon::now();
        
        DB::table('clients')
                ->where('author_id', '=', $aPost['author_id'])
                ->where('id', '=', $aPost['options']['id'])
                ->update($aData);
        
        //далее обновление дополнительных полей
        //удаление всех записей, с морды нет передачи id для таблицы clients_fields, поэтому проверять не с чем
        ClientField::where('author_id', '=', $aPost['author_id'])
                ->where('client_id', '=', $aPost['options']['id'])
                ->delete();
        
        //DB::beginTransaction();
        
        $aData = null;
        
        if (!empty($aPost['options']['contact_fields']))
        {
            foreach ($aPost['options']['contact_fields'] as $key => $aVal)
            {
                $aData[] = [
                    'author_id' => $aPost['author_id'],
                    'client_id' => $aPost['options']['id'],
                    'field_group' => empty($aVal['field_group']) ? '' : $aVal['field_group'],
                    'field_name' => empty($aVal['field_name']) ? '' : htmlspecialchars($aVal['field_name']),
                    'field_value' => empty($aVal['field_value']) ? '' : htmlspecialchars($aVal['field_value']),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            //DB::commit();

            //добавление новых записей
            DB::table('clients_fields')->insert($aData);
        }
        
        print json_encode(['result' => 1]);
        
        return;
    }
    
    /*** работа с группами клиентов/контактов ***/
    
    public static function getGroupsList($aPost)
    {
        if (!empty($aPost['author_id']))
        {
            //подсчёт количества записей
            $oGroupsCount = DB::select('SELECT clients_user_groups.*, 
                (SELECT COUNT(*) FROM clients WHERE clients.group_id = clients_user_groups.id AND author_id = ?  ) AS item_count
                FROM clients_user_groups WHERE author_id = ?  ORDER BY sorting ASC', [$aPost['author_id'], $aPost['author_id']]);
            
            //перекладываю в одномерный массив
            foreach ($oGroupsCount as $key => $oVal)
            {
                $aGroupCountId[$oVal->id] = $oVal->item_count;
            }
            
            $aGroups = ClientUserGroup::where('author_id', '=', $aPost['author_id'])
                    ->where('parent_id', '=', 0)
                    ->orderBy('sorting', 'asc')
                    ->get()
                    ->toArray();

            foreach ($aGroups as $key => $aVal)
            {
                $aId[] = $aVal['id'];
                $aResult[$aVal['id']] = $aVal;
                $aResult[$aVal['id']]['item_count'] = $aGroupCountId[$aVal['id']];
            }

            $aSubGroups = ClientUserGroup::where('author_id', '=', $aPost['author_id'])
                    ->whereIn('parent_id', $aId)
                    ->orderBy('sorting', 'ASC')
                    ->get()
                    ->toArray();

            $k = 0;
            
            foreach ($aSubGroups as $key => $aVal)
            {
                $aResult[$aVal['parent_id']]['sub_groups'][$k] = $aVal;
                $aResult[$aVal['parent_id']]['sub_groups'][$k]['item_count'] = $aGroupCountId[$aVal['id']];
                $k++;
            }
            
            //на стороне JS принимается почему то объект, а не массив, поэтому тут преобразовываем
            //а именно делаем нумерацию ключей в массивах с 0 и по порядку
            $k = 0;
            foreach ($aResult as $key => $aVal)
            {
                $aResult2[$k] = $aVal;
                
                $n = 0;
                foreach ($aVal['sub_groups'] as $keySub => $aValSub)
                {
                    unset($aResult2[$k]['sub_groups'][$keySub]);
                    $aResult2[$k]['sub_groups'][$n] = $aValSub;
                    $n++;
                }
                
                $k++;
            }
            
            $aResultMethod = ['result' => 1, 'data' => $aResult2];
        }
        else $aResultMethod = ['result' => 0];
        
        print json_encode($aResultMethod);
        
        return;
    }
    
    //нужен рефакторинг - обновление пачкой, вынести обновление из цикла
    public static function updateGroupsList($aPost)
    {
        if (!empty($aPost['author_id']))
        {
            $aId = [];
            $aNewItems = [];
            
            //обновление существующих групп
            foreach ($aPost['options']['groups_list'] as $key => $aVal)
            {
                $aData = [
                    'author_id' => $aPost['author_id'],
                    'parent_id' => $aVal['parent_id'],
                    'name' => htmlspecialchars($aVal['name']),
                    'sorting' => $aVal['sorting'],
                    //'icon' => empty($aVal['icon']) ? '' : htmlspecialchars($aVal['icon']),
                    //'type_item' => empty($aVal['type_item']) ? '' : htmlspecialchars($aVal['type_item']),
                ];
                
                $nResultUpdate = ClientUserGroup::where('author_id', '=', $aPost['author_id'])
                        ->where('id', '=', $aVal['id'])
                        ->update($aData);
                
                if ($nResultUpdate == 0) $aNewItems[] = $aData;
                
                $aData = null;
                $aId[] = $aVal['id'];
                $nResultUpdate = null;
                
                //обновление существующих ПОДгрупп
                foreach ($aPost['options']['groups_list'][$key]['sub_groups'] as $key_sub => $aValSub)
                {
                    $aData = [
                        'author_id' => $aPost['author_id'],
                        'parent_id' => $aValSub['parent_id'],
                        'name' => htmlspecialchars($aValSub['name']),
                        'sorting' => $aValSub['sorting'],
                        //'icon' => empty($aVal['icon']) ? '' : htmlspecialchars($aVal['icon']),
                        //'type_item' => empty($aVal['type_item']) ? '' : htmlspecialchars($aVal['type_item']),
                    ];

                    $nResultUpdate = ClientUserGroup::where('author_id', '=', $aPost['author_id'])
                            ->where('id', '=', $aValSub['id'])
                            ->update($aData);

                    if ($nResultUpdate == 0) $aNewItems[] = $aData;

                    $aData = null;
                    $aId[] = $aValSub['id'];
                }
                
                $nResultUpdate = null;
            }
            
            //удаление старых записей
            ClientUserGroup::where('author_id', '=', $aPost['author_id'])
                    ->whereNotIn('id', $aId)
                    ->delete();
            
            //добавление новых записей
            if (!empty($aNewItems)) DB::table('clients_user_groups')->insert($aNewItems);
                    
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    
    public static function setUserGroups($aPost)
    {
        if (!empty($aPost['author_id']))
        {
            $oClientUserGroups = ClientUserGroup::where('author_id', '=', $aPost['author_id'])->first();
            
            if (empty($oClientUserGroups->id))
            {
                $aGroups = ClientGroup::where('parent_id', '=', 0)
                        ->get()
                        ->toArray();
                
                foreach ($aGroups as $key => $aVal)
                {
                    $aResult = [
                        'author_id' => $aPost['author_id'],
                        'parent_id' => 0,
                        'name' => htmlspecialchars($aVal['name']),
                        'icon' => htmlspecialchars($aVal['icon']),
                        'type_item' => htmlspecialchars($aVal['type_item']),
                        //'created_at' => Carbon::now(),
                        //'updated_at' => Carbon::now(),
                    ];
                    
                    ClientUserGroup::create($aResult);
                    
                    $nParentId = ClientUserGroup::all()->last()->id;
                    
                    $aSubGroups = ClientGroup::where('parent_id', '=', $aVal['id'])
                            ->get()
                            ->toArray();
                    
                    $aResult = null;
                    
                    foreach ($aSubGroups as $key => $aVal)
                    {
                        $aResult[] = [
                            'author_id' => $aPost['author_id'],
                            'parent_id' => $nParentId,
                            'name' => htmlspecialchars($aVal['name']),
                            'icon' => htmlspecialchars($aVal['icon']),
                            'type_item' => htmlspecialchars($aVal['type_item']),
                            //'sorting' => $nSorting,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                    }

                    DB::table('clients_user_groups')->insert($aResult);
                }
                
            }
        }
        
        return;
    }
    
    /*** Комментарии ***/
    
    public static function addComments($aPost)
    {
        $oComment = new Comment();
        
        if (!empty($aPost['author_id']) && !empty($aPost['options']['id']))
        {
            $aData = [
                'author_id' => $aPost['author_id'],
                'client_id' => $aPost['options']['id'],
                'comment_text' => htmlspecialchars($aPost['options']['comment_text']),
            ];

            $oComment->create($aData);
            
            print json_encode(['result' => 1]);
        }
        else print json_encode(['result' => 0]);
        
        return;
    }
    
    public static function getCommentsList($aPost)
    {
        if (!empty($aPost['author_id']) && !empty($aPost['options']['id']))
        {
            $aCommentList = Comment::where('author_id', '=', $aPost['author_id'])
                    ->where('client_id', '=', $aPost['options']['id'])
                    ->orderBy('id', 'DESC')
                    ->get()
                    ->toArray();
            
            if (!empty($aCommentList)) $aResult = ['result' => 1, 'data' => $aCommentList];
            else 
            {
                $aResult = ['result' => 1, 'data' => []];
            }
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    /* *** */
    
    public static function isClient($aPost, Client $oClient)
    {
        $aData = Client::where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->first()->toArray();
        
        if (empty($aData))
        {
            $aResult = ['result' => 0, 'data' => ['result' => 0]];
        }
        else $aResult = ['result' => 1, 'data' => ['result' => 1]];
        
        print json_encode($aResult);
        
        return;
    }
    
//    public static function serachClient($aPost, Client $oClient)
//    {
//        $aData = Client::where('author_id', '=', $aPost['author_id'])
//                ->where('name', 'like', '%'.$aPost['options']['search_name'].'%')
//                ->get()
//                ->toArray();
//        
//        $aResult = ['result' => 1, 'data' => $aData];
//        
//        print json_encode($aResult);
//        
//        return;
//    }
    
    /*** Произвольные поля для клиента ***/
    
    public static function getFields($aPost, Client $oClient)
    {
        if (!empty($aPost['author_id']) && !empty($aPost['options']['id']))
        {
            $aData = ClientField::where('author_id', '=', $aPost['author_id'])
                    ->where('client_id', '=', $aPost['options']['id'])
                    ->where('field_group', '=', $aPost['options']['field_group'])
                    ->get()
                    ->toArray();
            
            if (!empty($aData)) $aResult = ['result' => 1, 'data' => $aData];
            else $aResult = ['result' => 1, 'data' => [] ];
        }
        else $aResult = ['result' => 0];
            
        print json_encode($aResult);
        
        return;
    }
    
    public static function updateFields($aPost, Client $oClient)
    {
        //в этом методе: добавление, удаление, обновление
        
        if (!empty($aPost['author_id']) && !empty($aPost['options']['id']))
        {
            //удалние сначало всех записаей
            ClientField::where('author_id', '=', $aPost['author_id'])
                    ->where('client_id', '=', $aPost['options']['client_id'])
                    ->delete();
            
            foreach ($aPost['options']['fields_list'] as $key => $aVal)
            {
                $aData[] = [
                    'author_id' => $aPost['author_id'],
                    'client_id' => $aPost['options']['client_id'],
                    'field_group' => $aVal['field_group'],
                    'field_name' => htmlspecialchars($aVal['field_name']),
                    'field_value' => htmlspecialchars($aVal['field_value']),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
            
            DB::table('clients_fields')->insert($aData);
                    
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    /*** ----------------------------- ***/
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
