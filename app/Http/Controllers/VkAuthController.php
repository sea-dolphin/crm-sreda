<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use Carbon\Carbon;

use App\Models\UserToken;

use App\Http\Controllers\OrdersController;
use App\Http\Controllers\ClientsController;

class VkAuthController extends Controller
{
    public function login($aPost)
    {
        //заполнение статусами заказов/сделок личный список пользователя
        //и других личных списков
        $this->setUserWishList($aPost);
        //-----------------------
            
        if (!empty($aPost['author_id']) && !empty($aPost['options']['password']))
        {
            $oData = UserToken::where('author_id', '=', $aPost['author_id'])
                    ->where('password', '=', $aPost['options']['password'])
                    ->first();
            
            if (!empty($oData->id)) $aResult = ['result' => 1, 'data' => ['sreda_key' => $oData->sreda_key, 'code' => 1] ];
            else $aResult = ['result' => 1, 'data' => ['code' => 0] ];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function register($aPost)
    {
        //dd($aPost);
        if (!empty($aPost['author_id']) && !empty($aPost['options']['password']))
        {
            $strSredaKey = md5('::'.$aPost['author_id'].'::'.$aPost['options']['password'].'::'.time());
            
            $aData = [
                'author_id' => $aPost['author_id'],
                'sreda_key' => $strSredaKey,
                'social_network' => 'vk',
                'password' => $aPost['options']['password'],
            ];
            
            UserToken::create($aData);
            
            //заполнение статусами заказов/сделок личный список пользователя
            //и других личных списков
            $this->setUserWishList($aPost);
            //-----------------------
            
            $aResult = ['result' => 1, 'data' => ['sreda_key' => $strSredaKey] ];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function userExist($aPost)
    {
        //dd($aPost);
        $oData = UserToken::where('author_id', '=', $aPost['author_id'])
                ->first();
        
        if (empty($oData->id))
        {
            $aResult = [
                'result' => 1, 
                'data' => [
                    'author_id' => $aPost['author_id'],
                    //'message' => 'Пользователя не существует', 
                    'code' => 0,
                ],
            ];
        }
        else 
        {
            $oData = UserToken::where('author_id', '=', $aPost['author_id'])
                    ->where('sreda_key', '=', $aPost['sreda_key'])
                    ->first();
            
            if (empty($oData->id)) 
            {
                $aResult = [
                    'result' => 1, 
                    'data' => [
                        'author_id' => $aPost['author_id'],
                        //'message' => 'Необходима авторизация, но пользователь существует', 
                        'code' => 1,
                    ] 
                ];
            }
            else 
            {
                $aResult = [
                    'result' => 1, 
                    'data' => [
                        'author_id' => $aPost['author_id'],
                        //'message' => 'Пользователь авторизован', 
                        'code' => 2,
                    ] 
                ];
            }
            
        }
        
        print json_encode($aResult);
        
        return;
    }
    
    public function userOnline($aPost)
    {
        if (!empty($aPost['author_id']) && !empty($aPost['sreda_key']))
        {
            $oData = UserToken::where('author_id', '=', $aPost['author_id'])
                    ->where('sreda_key', '=', $aPost['sreda_key'])
                    ->first();
            
            if (empty($oData->id)) 
            {
                return false;
            }
            else 
            {
                return true;
            }
        }
        else return false;
    }
    
    private function setUserWishList($aPost)
    {
        $oOrdersController = new OrdersController();
        $oOrdersController->setOrderUserStatus($aPost);
        
        ClientsController::setUserGroups($aPost);
        
        return true;
    }
    
}
