<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;

use App\Models\Payment;

class PaymentsController extends Controller
{
    private $aConfig;
    
    public function __construct() 
    {
        $this->aConfig = [
            'shopId' => env('shopId'),
            'scId' => env('scId'),
            'ShopPassword' => env('ShopPassword'),
        ];
    }
    
    public function paymentPage(Request $oRequest)
    {
        $nAuthorId = $oRequest->input('author_id');
        
        $oData = DB::table('tariffs')->get();
        
        return view('payment.payment', ['aConfig' => $this->aConfig, 'nAuthorId' => $nAuthorId, 'oData' => $oData]);
    }
    
    public function check(Request $oRequest)
    {
        $hash = md5($oRequest->input('action').';'.$oRequest->input('orderSumAmount').';'.$oRequest->input('orderSumCurrencyPaycash').';'.$oRequest->input('orderSumBankPaycash').';'.
                $this->aConfig['shopId'].';'.$oRequest->input('invoiceId').';'.$oRequest->input('customerNumber').';'.$this->aConfig['ShopPassword']);
        
	if (strtolower($hash) != strtolower($oRequest->input('md5')))
        {
            $code = 1;
	}
	else 
        {
            $code = 0;
	}
        
        print '<?xml version="1.0" encoding="UTF-8"?>';
        print '<checkOrderResponse performedDatetime="'. $oRequest->input('requestDatetime') .'" code="'.$code.'"'. ' invoiceId="'. $oRequest->input('invoiceId') .'" shopId="'. $this->aConfig['shopId'] .'"/>';
        
        return;
    }
    
    public function aviso(Request $oRequest)
    {
        $strAction = empty($oRequest->input('action')) ? '' : $oRequest->input('action');
        $nOrderSumAmount = empty($oRequest->input('orderSumAmount')) ? 0 : $oRequest->input('orderSumAmount');
        $nOrderSumCurrencyPaycash = empty($oRequest->input('orderSumCurrencyPaycash')) ? 0 : $oRequest->input('orderSumCurrencyPaycash');
        $strOrderSumBankPaycash = empty($oRequest->input('orderSumBankPaycash')) ? 0 : $oRequest->input('orderSumBankPaycash');
        $nInvoiceId = empty($oRequest->input('invoiceId')) ? 0 : $oRequest->input('invoiceId');
        $nCustomerNumber = empty($oRequest->input('customerNumber')) ? 0 : $oRequest->input('customerNumber');
        $strRequestDatetime = empty($oRequest->input('requestDatetime')) ? '' : $oRequest->input('requestDatetime');
        
        $strMD5 = $oRequest->input('md5');
        
        $strMessage = empty($oRequest->input('message')) ? '' : $oRequest->input('message');
        $strTechMessage = empty($oRequest->input('techMessage')) ? '' : $oRequest->input('techMessage');
        $nOrderNumber = empty($oRequest->input('orderNumber')) ? 0 : $oRequest->input('orderNumber');
        $strPaymentType = empty($oRequest->input('paymentType')) ? '' : $oRequest->input('paymentType');
        $nShopSumAmount = empty($oRequest->input('shopSumAmount')) ? 0 : $oRequest->input('shopSumAmount');
        
        $hash = md5($strAction.';'.$nOrderSumAmount.';'.$nOrderSumCurrencyPaycash.';'.$strOrderSumBankPaycash.';'.$this->aConfig['shopId'].';'.$nInvoiceId.';'.$nCustomerNumber.';'.$this->aConfig['ShopPassword']);
	
        if (strtolower($hash) != strtolower($strMD5))
        { 
            $code = 1;
	}
	else 
        {
            //успешно
            $code = 0;
            
            //фиксация платежа с дополнительными параметрами, изначально фиксация была в AjaxController@setOrder
            $aData = [
                'status' => 1,
                'invoiceId' => $nInvoiceId,
                'orderSumAmount' => $nOrderSumAmount,
                'message' => $strMessage,
                'techMessage' => $strTechMessage,
                'orderSumCurrencyPaycash' => $nOrderSumCurrencyPaycash,
                'orderSumBankPaycash' => $strOrderSumBankPaycash,
                'requestDatetime' => $strRequestDatetime,
                'paymentType' => $strPaymentType,
                'shopSumAmount' => $nShopSumAmount,
            ];
            
            //записываю данные о платеже от Яндекса
            Payment::where('id', '=', $nCustomerNumber)->update($aData);
            
	}
        
        print '<?xml version="1.0" encoding="UTF-8"?>';
        print '<paymentAvisoResponse performedDatetime="'. $strRequestDatetime .'" code="'.$code.'" invoiceId="'. $nInvoiceId .'" shopId="'. $this->aConfig['shopId'] .'"/>';
        //print '<paymentAvisoResponse performedDatetime="'. $_POST['requestDatetime'] .'" code="'.$code.'" invoiceId="'. $_POST['invoiceId'] .'" shopId="'. $this->aConfig['shopId'] .'"/>';

        return;
    }
    
    public function success()
    {
        return view('pages.page', ['strMessage' => 'Платёж удачно совершён']);
    }
    
    public function fail()
    {
        return view('pages.page', ['strMessage' => 'Ошибка платежа, обратитесь к администратору ресурса']);
    }
}
