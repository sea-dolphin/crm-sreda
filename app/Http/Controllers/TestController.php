<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use Carbon\Carbon;
use Symfony\Component\DomCrawler;

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\HtmlParserController;

use Maatwebsite\Excel\Facades\Excel as Excel123;

use App\Models\Client;
use App\Models\ClientStatus;
use App\Models\Order;
use App\Models\Catalog;
use App\Models\OrderStatus;
use App\Models\ClientGroup;
use App\Models\UserToken;
use App\Models\ClientUserGroup;
use App\Models\ClientField;

use Illuminate\Support\Facades\Mail;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Client $oClient, ClientStatus $oClientStatus)
    {
        //$oExcel = new Excel();
        //$excel2 = new \Maatwebsite\Excel\Excel;
        //$ex3 = new \Maatwebsite\Excel\Collections\ExcelCollection;
        //$ex4 = new \Maatwebsite\Excel\Parsers\ExcelParser;
        //$ex5 = new \Maatwebsite\Excel\ExcelServiceProvider;
        //$oExcel->load('temp/file.xls');
        //$excel2->load('temp/file.xls');
        //$excel2->create('123.xls');
        
//        \Maatwebsite\Excel\Facades\Excel::create('Laravel Excel', function($excel) {
//
//            $excel->sheet('Excel sheet', function($sheet) {
//
//                $sheet->setOrientation('landscape');
//
//            });
//
//        })->export('xls');
        
//        \Maatwebsite\Excel\Facades\Excel::load('temp/file.xls', function($reader) {
//
//            // reader methods
//
//        });
        
        Excel123::load('temp/file.xls', function($reader) {

            // reader methods
            $result = $reader->get();
            foreach ($result as $key => $value) 
            {
                //для проверки можешь с перва вывести так посмотреть берутся ли значения 
                //echo $value->date.'----'.$value->name.'----'.$value->adress_to.'----'.$value->operator.'<br>';

                //Model->твоя модель
                //dd($value[0]->name);
                //print $value[0]->name.'<br>';
                foreach ($value as $key2 => $val2)
                {
                    //dd($val2);
                    print $val2->name.'<br>';
                }
            }

        })->get();
    }
    
    public function test2()
    {
        $x = 50;
        $y = 20;
        
        $x1 = 60;
        $y1 = 17;
        
        if ($x1 > $x || $y1 > $y) print 'false';
        else print 'true';
        
//        $aData = Client::all()->toArray();
//        
//        $aClientFields = [
//            'city' => 'Город',
//            'phone' => 'Тел. моб.',
//            'work_phone' => 'Тел. раб.',
//            'site' => 'Сайт',
//            'email' => 'Email',
//            'skype' => 'Skype',
//        ];
//        
//        $aClientFields2 = [
//            'Город' => 'city',
//            'Тел. моб.' => 'phone',
//            'Тел. раб.' => 'work_phone',
//            'Сайт' => 'site',
//            'Email' => 'email',
//            'Skype' => 'skype',
//        ];
//        
//        foreach ($aData as $key => $aValClient)
//        {
//            $aDataInsert[] = [
//                'author_id' => $aValClient['author_id'],
//                'client_id' => $aValClient['client_id'],
//                'field_name' => 'Город',
//                'field_value' => $aValClient['city'],
//            ];
//            
//            $aDataInsert[] = [
//                'author_id' => $aValClient['author_id'],
//                'client_id' => $aValClient['client_id'],
//                'field_name' => 'Тел. моб.',
//                'field_value' => $aValClient['phone'],
//            ];
//            
//            $aDataInsert[] = [
//                'author_id' => $aValClient['author_id'],
//                'client_id' => $aValClient['client_id'],
//                'field_name' => 'Тел. раб.',
//                'field_value' => $aValClient['work_phone'],
//            ];
//            
//            $aDataInsert[] = [
//                'author_id' => $aValClient['author_id'],
//                'client_id' => $aValClient['client_id'],
//                'field_name' => 'Место работы',
//                'field_value' => $aValClient['place_of_work'],
//            ];
//            
//            $aDataInsert[] = [
//                'author_id' => $aValClient['author_id'],
//                'client_id' => $aValClient['client_id'],
//                'field_name' => 'Сайт',
//                'field_value' => $aValClient['site'],
//            ];
//            
//            $aDataInsert[] = [
//                'author_id' => $aValClient['author_id'],
//                'client_id' => $aValClient['client_id'],
//                'field_name' => 'Email',
//                'field_value' => $aValClient['email'],
//            ];
//            
//            $aDataInsert[] = [
//                'author_id' => $aValClient['author_id'],
//                'client_id' => $aValClient['client_id'],
//                'field_name' => 'Skype',
//                'field_value' => $aValClient['skype'],
//            ];
//            
//            //ClientField::create($aDataInsert);
//            DB::table('clients_fields')->insert($aDataInsert);
//            $aDataInsert = null;
//        }
        
//        foreach ($aData as $key => $aValClient)
//        {
//            $oFields = ClientField::where('author_id', '=', $aValClient['author_id'])->get();
//            
//            foreach ($oFields as $key => $oValField)
//            {
//                //обновление существующих записей
//                foreach ($aClientFields as $key => $strVal)
//                {
//                    if (!empty($aValClient[$key]) && $oValField->field_name == $strVal && empty($oValField->field_value))
//                    {
//                        ClientField::where('author_id', '=', $aValClient['author_id'])->where('field_name', '=', $oValField->field_name)->update(['field_value' => $aValClient[$key]]);
//                    }
//                    
//                }
//                
//                $aFieldsExist[] = $oValField->field_name;
//                $aFieldsExistValue[$oValField->field_name] = $oValField->field_value;
//            }
//            
//            //добавление полей
//            if (!empty($aFieldsExist))
//            {
//                foreach ($aClientFields as $key => $strVal)
//                {
//                    if (!in_array($strVal, $aFieldsExist))
//                    {
//                        $aDataInsert = [
//                            'field_name' => empty($strVal) ? '' : $strVal,
//                            'field_value' => empty($strVal) ? '' : $aFieldsExistValue[$strVal],
//                        ];
//
//                        ClientField::create($aDataInsert);
//                    }
//                }
//            }
//                    
//            $aFieldsExist = null;
//        }
        return;
    }
    
    
    
    
    
    
    public function _curl($strHost,$mixData=false,$bHeaders=false,$strAuth=''){
            $oCurl = curl_init();
            if($oCurl) {
                    if($strAuth){
                            curl_setopt($oCurl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);       
                            curl_setopt($oCurl, CURLOPT_USERPWD, $strAuth);
                    }
            if($mixData){
                            //$mixData = (is_array($mixData)) ? http_build_query($mixData,'','&') : $mixData;

            curl_setopt($oCurl,CURLOPT_SSL_VERIFYPEER,false);
            curl_setopt($oCurl,CURLOPT_SSL_VERIFYHOST,false);
            curl_setopt($oCurl,CURLOPT_URL,$strHost);
            curl_setopt($oCurl,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($oCurl,CURLOPT_POST,false);
            curl_setopt($oCurl,CURLOPT_POSTFIELDS,$mixData);
            curl_setopt($oCurl,CURLOPT_HEADER,$bHeaders);
            $oResult = curl_exec($oCurl);
            curl_close($oCurl);

            return $oResult;
            }else{
                            curl_setopt($oCurl, CURLOPT_HTTPGET, 1);
                            curl_setopt($oCurl, CURLOPT_URL, $strHost);
                    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER,true);
                    $oResult = curl_exec($oCurl);
            curl_close($oCurl);

            return $oResult;
                    }
            }
    return false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    //спешка вредна ... пример спешки мне из прошлого
    public function updateOrder2($aPost)
    {
        if (!empty($aPost['author_id']) && !empty($aPost['options']['order_id']))
        {
            if ($aPost['options']['order_id'] == 0)
            {
                $aData = [
                    'comment' => empty($aPost['options']['comment']) ? '' : $aPost['options']['comment'],
                    'updated_at' => Carbon::now(),
                ];

                DB::table('orders')->where('author_id', '=', $aPost['author_id'])->where('id', '=',$aPost['options']['order_id'])->update($aData);
            }
            else
            {
                //тут в цикле проверка, если id такой есть с параметром amount > 0, то обновляем, если id новый, то добавляю, 
                //если в списке id нет, а в базе есть, то удаляю.
                foreach ($aPost['options']['products_list'] as $key => $aVal)
                {
                    $aId[ $aPost['options']['products_list'][$key]['product_id'] ] = $aPost['options']['products_list'][$key]['product_id'];
                    $aPrices['price'][ $aPost['options']['products_list'][$key]['product_id'] ] = $aPost['options']['products_list'][$key]['price'];
                    $aPrices['amount'][ $aPost['options']['products_list'][$key]['product_id'] ] = $aPost['options']['products_list'][$key]['amount'];
                }
                
                $oOrderItems = Order::whereIn('id', $aId)->get();
                $nTotalAmount = 0;
                $nTotalPrice = 0;
                
                foreach ($oOrderItems as $val)
                {
                    $val->amount = $aPrices['amount'][$val->id];
                    $val->price = $aPrices['price'][$val->id];
                    $val->total_ptrice = $val->amount * $val->price;
                    $val->save();
                    
                    $nTotalAmount += $val->amount;
                    $nTotalPrice += $val->total_ptrice;
                }
                
                Order::where('id', '=', $aPost['options']['order_id'])->update(['amount' => $nTotalAmount, 'total_price' => $nTotalPrice]);
            }
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
}
