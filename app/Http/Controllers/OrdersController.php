<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use Carbon\Carbon;

use App\Models\Order;
use App\Models\Catalog;
use App\Models\OrderUserStatus;
use App\Models\OrderStatus;

use App\Http\Controllers\SiteController;

class OrdersController extends Controller
{
    private $oOrderModel;
    
    public function __construct() {
        
        $this->oOrderModel = new Order();
    }
    
    public function addOrder($aPost)
    {
        //изначально проверяю на лимиты, только потом пропускаю
//        $bLimit = SiteController::bCheckPayLimit($aPost, 'orders', 20);
//        
//        if (!$bLimit)
//        {
//            print json_encode(['result' => 0, 'message' => 'Превышен лимит, либо период оплаты исчерпан']);
//            return;
//        }
        //-------
        
        if (!empty($aPost['author_id']) && !empty($aPost['options']['client_id']))
        {
            //создание заказа, в том числе внесение товаров
            
            $aData = [
                'author_id' => $aPost['author_id'],
                'client_id' => $aPost['options']['client_id'],
                'status_id' => $aPost['options']['status_id'],
                'comment' => empty($aPost['options']['comment']) ? '' : htmlspecialchars($aPost['options']['comment']),
                'delivery_address' => empty($aPost['options']['delivery_address']) ? '' : htmlspecialchars($aPost['options']['delivery_address']),
                'budget' => empty($aPost['options']['budget']) ? 0 : htmlspecialchars($aPost['options']['budget']),
                'order_name' => empty($aPost['options']['order_name']) ? '' : htmlspecialchars($aPost['options']['order_name']),
            ];

            $this->oOrderModel->create($aData);

            $nLastId = $this->oOrderModel->all()->last()->id;
            $aData = null;
            
            if (!empty($aPost['options']['products_list']))
            {
                foreach ($aPost['options']['products_list'] as $key => $aVal)
                {
                    $aId[] = $aPost['options']['products_list'][$key]['product_id'];
                }

                //для внесения стоимости в таблицу orders
                $aCatalog = Catalog::whereIn('id', $aId)
                        ->get()
                        ->toArray();

                //нужно преобразовать в одномерный массив, с ключом id
                foreach ($aCatalog as $key => $aVal)
                {
                    $aProducts[$aVal['id']] = $aVal['price'];
                }

                $nTotalAmount = 0;
                $nTotalPrice = 0;

                //здесь перебор массива по переданным товарам, запись их в базу
                foreach ($aPost['options']['products_list'] as $key => $aVal)
                {
                    $aData[] = [
                        'author_id' => $aPost['author_id'],
                        'client_id' => $aPost['options']['client_id'],
                        'status_id' => $aPost['options']['status_id'],
                        'order_id' => $nLastId,
                        'product_id' => $aPost['options']['products_list'][$key]['product_id'],
                        'amount' => $aPost['options']['products_list'][$key]['amount'],
                        'price' => $aProducts[$aPost['options']['products_list'][$key]['product_id']],
                        'total_price' => $aProducts[$aPost['options']['products_list'][$key]['product_id']] * $aPost['options']['products_list'][$key]['amount'],
                    ];

                    $nTotalAmount += $aPost['options']['products_list'][$key]['amount'];
                    $nTotalPrice  += $aProducts[$aPost['options']['products_list'][$key]['product_id']] * $aPost['options']['products_list'][$key]['amount'];
                }

                DB::table('orders')->insert($aData);

                //обновляю через модель, для разнообразия
                $oOrder = Order::find($nLastId);
                $oOrder->amount = $nTotalAmount;
                $oOrder->total_price = $nTotalPrice;
                $oOrder->save();
            }
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }

    public function getOrdersList($aPost)
    {
        //dd($aPost);
        $nStatusId = empty($aPost['options']['status_id']) ? 0 : intval($aPost['options']['status_id']);
        $nClientId = empty($aPost['options']['client_id']) ? 0 : intval($aPost['options']['client_id']);
        
        $strMark = '';
        
        if ($nClientId == 0) $strMark = '>';
        else $strMark = '=';
        
        if ($nStatusId == 0) 
        {
            $aOrders = Order::with('get_client')
                    ->where('author_id', '=', $aPost['author_id'])
                    ->where('order_id', '=', 0)
                    ->where('client_id', $strMark, $nClientId)
                    ->orderBy('id', 'desc')
                    ->get()
                    ->toArray();
        }
        else
        {
            $aOrders = Order::with('get_client')
                    ->where('author_id', '=', $aPost['author_id'])
                    ->where('status_id', '=', $nStatusId)
                    ->where('order_id', '=', 0)
                    ->where('client_id', $strMark, $nClientId)
                    ->orderBy('id', 'desc')
                    ->get()
                    ->toArray();
        }
        
        if (!empty($aOrders)) $aResult = ['result' => 1, 'data' => $aOrders];
        else $aResult = ['result' => 1, 'data' => []];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getOrderData($aPost)
    {   
        $aOrder = Order::with('product_one')
                ->with('get_client')
                ->where('author_id', '=', $aPost['author_id'])
                ->where('order_id', '=', $aPost['options']['order_id']) //74
                //->whereRaw(' order_id = ? OR id = ? ', [$aPost['options']['order_id'], $aPost['options']['order_id']])
                ->get()
                ->toArray();
        
        $aOrderParent = Order::with('product_one')
                ->with('get_client')
                ->where('author_id', '=', $aPost['author_id'])
                ->where('id', '=', $aPost['options']['order_id']) //74
                //->get()
                ->first()
                ->toArray();
        
        //$aOrder['parent_order'] = $aOrderParent;
        $aOrderParent['positions'] = $aOrder;
        
        if (!empty($aOrderParent)) $aResult = ['result' => 1, 'data' => $aOrderParent];
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getStatusList($aPost)
    {
        //$aData = DB::table('orders_status')->get()->toArray();
        
        $oStatusCount = DB::select('SELECT orders_user_statuses.*, 
            (SELECT COUNT(*) FROM orders WHERE orders.status_id = orders_user_statuses.id AND author_id = ?  ) AS item_count
            FROM orders_user_statuses WHERE author_id = ? ORDER BY sorting ASC ', [$aPost['author_id'], $aPost['author_id']]);
        
        if (!empty($oStatusCount)) 
        {
            $aResult = ['result' => 1, 'data' => $oStatusCount];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function deleteProductFromOrder($aPost)
    {
        if (!empty($aPost['author_id']) && !empty($aPost['options']['product_id']))
        {
            DB::table('orders')->where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['product_id'])->delete();
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function updateOrder($aPost)
    {
        //dd($aPost);
        //удаляю все позиции внутри заказа и добавляю новые
        if (!empty($aPost['author_id']) && !empty($aPost['options']['order_id']))
        {
            Order::where('order_id', '=', $aPost['options']['order_id'])->delete();
            
            if (!empty($aPost['options']['status_id'])) 
            {
                $aOrder = [
                    'client_id' => $aPost['options']['client_id'],
                    'status_id' => $aPost['options']['status_id'],
                    'amount' => 0,
                    'price' => 0,
                    'total_price' => 0,
                    'comment' => htmlspecialchars($aPost['options']['comment']),
                    'delivery_address' => htmlspecialchars($aPost['options']['delivery_address']),
                    'budget' => empty($aPost['options']['budget']) ? 0 : htmlspecialchars($aPost['options']['budget']),
                    'order_name' => empty($aPost['options']['order_name']) ? '' : htmlspecialchars($aPost['options']['order_name']),
                ];
                
                Order::where('id', '=', $aPost['options']['order_id'])
                        ->where('author_id', '=', $aPost['author_id'])
                        ->update($aOrder);
            }
            
            //здесь обновление списка продукции
            if (!empty($aPost['options']['products_list']))
            {
                // ** этот копипаст вынести в отдельный метод, он же есть в добавлении
                foreach ($aPost['options']['products_list'] as $key => $aVal)
                {
                    $aId[] = $aPost['options']['products_list'][$key]['product_id'];
                }

                //для внесения стоимости в таблицу orders
                $aCatalog = Catalog::whereIn('id', $aId)
                        ->get()
                        ->toArray();

                //нужно преобразовать в одномерный массив, с ключом id
                foreach ($aCatalog as $key => $aVal)
                {
                    $aProducts[$aVal['id']] = $aVal['price'];
                }
                //**
                
                $nTotalAmount = 0;
                $nTotalPrice = 0;

                foreach ($aPost['options']['products_list'] as $key => $aVal)
                {
                    //dd($aVal);
                    $aData[] = [
                        'author_id' => $aPost['author_id'],
                        'client_id' => $aPost['options']['client_id'],
                        'status_id' => $aPost['options']['status_id'],
                        'order_id' => $aPost['options']['order_id'],
                        'product_id' => $aPost['options']['products_list'][$key]['product_id'],
                        'amount' => $aPost['options']['products_list'][$key]['amount'],
                        'price' => $aProducts[$aPost['options']['products_list'][$key]['product_id']],
                        'total_price' => $aProducts[$aPost['options']['products_list'][$key]['product_id']] * $aPost['options']['products_list'][$key]['amount'],
                    ];

                    $nTotalAmount += $aPost['options']['products_list'][$key]['amount'];
                    $nTotalPrice  += $aProducts[$aPost['options']['products_list'][$key]['product_id']] * $aPost['options']['products_list'][$key]['amount'];
                }

                DB::table('orders')->insert($aData);

                //обновляю сам заказ, либо удаляю его если в нём ничего нет
                if ($nTotalPrice == 0)
                {
                    Order::where('id', '=', $aPost['options']['order_id'])->delete();
                }
                else
                {
                    $aData = [
                        'amount' => $nTotalAmount,
                        'total_price' => $nTotalPrice,
                    ];

                    if (!empty($aPost['options']['comment'])) $aData['comment'] = htmlspecialchars($aPost['options']['comment']);
                    if (!empty($aPost['options']['delivery_address'])) $aData['delivery_address'] = htmlspecialchars($aPost['options']['delivery_address']);
                    if (!empty($aPost['options']['client_id'])) $aData['client_id'] = $aPost['options']['client_id'];
                    if (!empty($aPost['options']['budget'])) $aData['budget'] = htmlspecialchars($aPost['options']['budget']);
                    if (!empty($aPost['options']['order_name'])) $aData['order_name'] = htmlspecialchars($aPost['options']['order_name']);

                    Order::where('id', '=', $aPost['options']['order_id'])->update($aData);
                }
            
            }
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function updateStatus($aPost)
    {
        if (!empty($aPost['options']['status_id'])) 
        {
            Order::where('author_id', '=', $aPost['author_id'])
                    ->whereRaw(' (id = ? OR order_id = ?) ', [$aPost['options']['order_id'], $aPost['options']['order_id']])
                    ->update(['status_id' => $aPost['options']['status_id']]);
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function removeOrder($aPost)
    {
        Order::where('author_id', '=', $aPost['author_id'])
                ->whereRaw(' (id = ? OR order_id = ?) ', [$aPost['options']['order_id'], $aPost['options']['order_id']])
                ->delete();
         
        $aResult = ['result' => 1];
         
        print json_encode($aResult);
        
        return;
    }
    
    public function setOrderUserStatus($aPost)
    {
        //dd($aPost);
        if (!empty($aPost['author_id']))
        {
            $aOrderUserStatus = OrderUserStatus::where('author_id', '=', $aPost['author_id'])->first();
            
            if (empty($aOrderUserStatus->id))
            {
                $aData = OrderStatus::all()->toArray();
                
                foreach ($aData as $key => $aVal)
                {
                    $nSorting = empty($aVal['type_item']) ? 0 : $aVal['sorting'];
                    
                    $aResult[] = [
                        'author_id' => $aPost['author_id'],
                        'name' => htmlspecialchars($aVal['name']),
                        'icon' => htmlspecialchars($aVal['icon']),
                        'type_item' => htmlspecialchars($aVal['type_item']),
                        'sorting' => $nSorting,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }
                
                DB::table('orders_user_statuses')->insert($aResult);
            }
        }
        
        $aResult = ['result' => 1];
         
        //print json_encode($aResult);
        
        return;
    }
    
    //нужен рефакторинг - обновление статусов пачкой, вынести обновление из цикла
    public function updateListOrderUserStatus($aPost)
    {
        //в этом методе: добавление, удаление, обновление
        //dd($aPost);
        if (!empty($aPost['author_id']))
        {
            $aId = [];
            $aNewItems = [];
            
            //обновление существующих стутусов
            foreach ($aPost['options']['status_list'] as $key => $aVal)
            {
                $aData = [
                    'author_id' => $aPost['author_id'],
                    'name' => htmlspecialchars($aVal['name']),
                    'icon' => empty($aVal['icon']) ? '' : htmlspecialchars($aVal['icon']),
                    'type_item' => empty($aVal['type_item']) ? '' : htmlspecialchars($aVal['type_item']),
                    'sorting' => $aVal['sorting'],
                ];
                
                $nResultUpdate = OrderUserStatus::where('author_id', '=', $aPost['author_id'])
                        ->where('id', '=', $aVal['id'])
                        ->update($aData);
                
                if ($nResultUpdate == 0) $aNewItems[] = $aData;
                
                $aData = null;
                $aId[] = $aVal['id'];
            }
            
            //удаление старых записей
            OrderUserStatus::where('author_id', '=', $aPost['author_id'])
                    ->whereNotIn('id', $aId)
                    ->delete();
            
            //добавление новых записей
            if (!empty($aNewItems)) DB::table('orders_user_statuses')->insert($aNewItems);
                    
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    
}