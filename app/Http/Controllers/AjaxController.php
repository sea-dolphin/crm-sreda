<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Models\Payment;

class AjaxController extends Controller
{
    public function setOrder(Request $request)
    {
        $nTariffId = $request->input('tariff_id');
        $nPrice = $request->input('sum');
        $nAuthorId = $request->input('author_id');
        
        if (!empty($nTariffId))
        {
            $oTariff = DB::table('tariffs')
                    ->where('id', '=', $nTariffId)
                    ->first();
            
            $dtActionEnd = Carbon::now()->addMonth($oTariff->month_interval);
            
            $aData = [
                'author_id' => $nAuthorId,
                'tariff_id' => $nTariffId,
                'action_end' => $dtActionEnd,
                'price' => $nPrice,
            ];
            
            Payment::create($aData);
            
            print Payment::all()->last()->id;
        }
        else print 0;
        
        return;
    }
}
