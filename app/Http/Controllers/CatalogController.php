<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel as ExcelMy;

use App\Models\Catalog;

use App\Http\Controllers\HtmlParserController;
use App\Http\Controllers\VkApiController;

class CatalogController extends Controller
{
    private $oCatalogModel;
    
    public function __construct() {
        
        $this->oCatalogModel = new Catalog();
    }
    
    public function addItem($aPost)
    {
        if (!empty($aPost['author_id']) /* && !empty($aPost['options']['name']) */)
        {
            $aData = [
                'author_id' => $aPost['author_id'],
                'name' => htmlspecialchars($aPost['options']['name']),
                'description' => empty($aPost['options']['description']) ? '' : htmlspecialchars($aPost['options']['description']),
                'price' => empty($aPost['options']['price']) ? 0 : $aPost['options']['price'],
                'it_category' => $aPost['options']['it_category'],
                'category_id' => $aPost['options']['category_id'],
            ];
            
            $this->oCatalogModel->create($aData);
            
            $nLastId = $this->oCatalogModel->all()->last()->id;
            
            print json_encode(['result' => 1, 'data' => ['last_id' => $nLastId]]);
        }
        else print json_encode(['result' => 0]);
        
        return;
    }
    
    public function getCategoriesList($aPost)
    {
        $aCatalog = Catalog::where('author_id', '=', $aPost['author_id'])
                ->where('it_category', '=', 1)
                ->where('category_id', '=', 0)
                ->orderBy('my_sorting', 'asc')
                ->get()
                ->toArray();
        
        $nTotalCount = Catalog::where('author_id', '=', $aPost['author_id'])
                ->where('it_category', '=', 0)
                ->where('category_id', '>', 0)
                ->count();
        
        if (!empty($aCatalog)) 
        {
            $oProductsCount = DB::select('SELECT DISTINCT catalog.category_id as cat_id, 
                (SELECT COUNT(*) FROM catalog WHERE catalog.category_id = cat_id) 
                AS item_count 
                FROM catalog where catalog.category_id > 0');
            
            foreach ($aCatalog as $key => $aVal)
            {
                $aCatalog[$key]['item_count'] = 0;
                $aCatalog[$key]['total_count'] = $nTotalCount;

                foreach ($oProductsCount as $key2 => $oVal2)
                {
                    if ($aVal['id'] == $oVal2->cat_id) $aCatalog[$key]['item_count'] = $oVal2->item_count;
                }
            }
            
            $aResult = ['result' => 1, 'data' => $aCatalog];
        }
        else 
        {
            $aResult = ['result' => 1, 'data' => []];
        }
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getProductsList($aPost)
    {
        $nCategoryId = intval($aPost['options']['category_id']);
        
        if (empty($aPost['options']['order_method']) && empty($aPost['options']['order_field']))
        {
            $strField = 'id';
            $strOrder = 'DESC';
        }
        else
        {
            $strField = $aPost['options']['order_field'];
            $strOrder = $aPost['options']['order_method'];
        }
        
        if ($nCategoryId > 0)
        {
            $aProductList = Catalog::where('author_id', '=', $aPost['author_id'])
                    ->where('it_category', '=', 0)
                    ->where('category_id', '=', $aPost['options']['category_id'])
                    ->select('id', 'author_id', 'name', 'price', 'category_id', 'image') //ввиду того что браузер не съедает такой объём информации, нужно отдавать выборочно
                    ->orderBy($strField, $strOrder)
                    ->get()
                    ->toArray();
        }
        else
        {
            $aProductList = Catalog::where('author_id', '=', $aPost['author_id'])
                    ->where('it_category', '=', 0)
                    ->where('category_id', '>', 0)
                    ->select('id', 'author_id', 'name', 'price', 'category_id', 'image')
                    ->orderBy($strField, $strOrder)
                    ->get()
                    ->toArray();
        }
        
        //$aProductList['amount'] = count($aProductList);
        
        if (!empty($aProductList)) $aResult = ['result' => 1, 'data' => $aProductList];
        else $aResult = ['result' => 1, 'data' => []];
        //else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getProductData($aPost)
    {
        $aProduct = Catalog::where('author_id', '=', $aPost['author_id'])
                ->where('id', '=', $aPost['options']['id'])
                ->first()
                ->toArray();
        
        if (!empty($aProduct)) $aResult = ['result' => 1, 'data' => $aProduct];
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }

    public function updateItem($aPost)
    {
        $oCatlog = Catalog::where('author_id', '=', $aPost['author_id'])
                ->where('id', '=', $aPost['options']['id'])
                ->first();
        
        if (!empty($oCatlog))
        {
            $aCatalog = $oCatlog->toArray();
            
            $aData = [
                'author_id' => $aPost['author_id'],
                'name' => htmlspecialchars($aPost['options']['name']),
                'description' => empty($aPost['options']['description']) ? '' : htmlspecialchars($aPost['options']['description']),
                'price' => empty($aPost['options']['price']) ? 0 : $aPost['options']['price'],
                'it_category' => $aPost['options']['it_category'],
                'category_id' => $aPost['options']['category_id'],
            ];
            
            DB::table('catalog')->where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->update($aData);
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    //тут позже исправить обновление в цикле на обновление пачкой
    public function updateCategory($aPost)
    {
        //dd($aPost);
        //if (!empty($aPost['options']['categories_list']))
        if (!empty($aPost['author_id']))
        {
            $aId = [];
            $aNewItems = [];
            
            if (!empty($aPost['options']['categories_list']))
            {
                foreach ($aPost['options']['categories_list'] as $key => $aVal)
                {
                    $aData = [
                        'author_id' => $aPost['author_id'],
                        'name' => htmlspecialchars($aVal['name']),
                        'it_category' => 1,
                        'category_id' => 0,
                        'my_sorting' => $key,
                    ];

                    $nResultUpdate = Catalog::where('author_id', '=', $aPost['author_id'])
                            ->where('id', '=', $aVal['id'])
                            ->update($aData);

                    if ($nResultUpdate == 0) $aNewItems[] = $aData;

                    $aData = null;
                    $aId[] = $aVal['id'];
                }
            }
            
            //удаление старых записей
            Catalog::where('author_id', '=', $aPost['author_id'])
                    ->where('it_category', '=', 1)
                    ->whereNotIn('id', $aId)
                    ->delete();
            
            Catalog::where('author_id', '=', $aPost['author_id'])
                    ->where('it_category', '=', 0)
                    ->whereNotIn('category_id', $aId)
                    ->delete();
            
            //добавление новых записей
            if (!empty($aNewItems)) DB::table('catalog')->insert($aNewItems);
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function updateCategoryItem($aPost)
    {
        $oCatlog = Catalog::where('author_id', '=', $aPost['author_id'])
                ->where('id', '=', $aPost['options']['id'])
                ->first();
        
        if (!empty($oCatlog))
        {
            $aData = [
                'it_category' => 0,
                'category_id' => $aPost['options']['category_id'],
            ];
            
            //DB::table('catalog')->where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->update($aData);
            Catalog::where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->update($aData);
            
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function deleteItem($aPost)
    {
        $aCatalog = Catalog::where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->first()->toArray();
        
        if ($aCatalog['it_category'] == 1) DB::table('catalog')->where('author_id', '=', $aPost['author_id'])->where('category_id', '=', $aPost['options']['id'])->delete();
        
        DB::table('catalog')->where('author_id', '=', $aPost['author_id'])->where('id', '=', $aPost['options']['id'])->delete();
        
        $aResult = ['result' => 1];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function ApiImportFromVkShop($aPost)
    {
        $oVkApi = new VkApiController();
        $aResult = ['result' => 1, 'data' => ['url' => $oVkApi->getUrl($aPost)] ];
        print json_encode($aResult);
        
        return;
    }

    public function importFromExcel($aPost, Request $request)
    {
        $oFile = $request->file('import_file');
        
        if ($oFile->isValid() && !empty($aPost['options']['category_id']))
        {
            ExcelMy::load($oFile, function($reader) {
                
                $result = $reader->get();
                foreach ($result as $key => $value) 
                {
                    foreach ($value as $key2 => $oValCell)
                    {
                        $aData[] = [
                            'author_id' => $aPost['author_id'],
                            'category_id' => $aPost['options']['category_id'],
                            'name' => $oValCell->name,
                            'description' => $oValCell->description,
                            'price' => $oValCell->price,
                        ];
                        
                    }
                }
            })->get();
            
            DB::table('catalog')->insert($aData);
        
            $aResult = ['result' => 1];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
