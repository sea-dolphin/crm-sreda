<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use Carbon\Carbon;

use App\Models\Payment;

class SiteController extends Controller
{
    public static function bCheckPayLimit($aPost, $strTable, $nLimitCount)
    {
        //проверка на лимиты по оплате
        $oUserToken = UserToken::where('author_id', '=', $aPost['author_id'])->first();
        
        $nTimeReg = strtotime($oUserToken->created_at);
        $nEndFreeTime = $nTimeReg + 604800; // +7 суток
        $nNowTime = strtotime(Carbon::now()->toDateTimeString());
        
        //$nClientCount = DB::table('clients')->where('author_id', '=', $aPost['author_id'])->count();
        //$nOrdersCount = DB::table('orders')->where('author_id', '=', $aPost['author_id'])->count();
        
        $nItemCount = DB::table($strTable)->where('author_id', '=', $aPost['author_id'])->count();
        
        if ($nNowTime > $nEndFreeTime)
        {
            //если бесплатное время закончено, иду смотреть платежи, сравниваю с последней оплатой
            $oPayment = Payment::where('author_id', '=', $aPost['author_id'])
                    ->orderBy('id', 'desc')
                    ->first();

            //если нет оплаты, но не привышен лимит
            if (empty($oPayment->id) && $nItemCount < $nLimitCount )
            {
                return true;
            }
            //если нет оплаты, и привышен лимит
            if (empty($oPayment->id) && $nItemCount >= $nLimitCount )
            {
                return false;
            }
            //если оплата поступила
            if (!empty($oPayment->id))
            {
                //если текущее время меньше чем дата окончания тарифа то считаем что подписка действует
                if (strtotime(Carbon::now()->toDateTimeString()) < strtotime($oPayment->action_end)) 
                {
                    return true;
                }
                else 
                {
                    //если оплата Просрочена, но не превышен лимит
                    if ($nItemCount < $nLimitCount) return true;
                    
                    return false;
                }
            }
        }
        else return true;
        
    }
    
    public static function convertDate($strDate)
    {
        $aMonths = [
            'января' => '01',
            'февраля' => '02',
            'марта' => '03',
            'апреля' => '04',
            'мая' => '05',
            'июня' => '06',
            'июля' => '07',
            'августа' => '08',
            'сентября' => '09',
            'октября' => '10',
            'ноября' => '11',
            'декабря' => '12',
        ];
        
        $aDate = explode(' ', $strDate);
        
        if (strlen($aDate[0]) == 1) $aDate[0] = '0'.$aDate[0];
        
        $dtOutDate = $aDate[2].'-'.$aMonths[$aDate[1]].'-'.$aDate[0].' 00:00:00'; //'1970-01-01 03:00:00'
        
        return $dtOutDate;
    }
    
    
}
