<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use Carbon\Carbon;

use App\Models\Setting;
use App\Models\Catalog;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\UserToken;

use App\Http\Controllers\VkApiController;

class SettingsController extends Controller
{
    public function vkInit($aPost)
    {
        if (!empty($aPost['author_id']))
        {
            $oVkApiController = new VkApiController();
            
            $aResult = ['result' => 1, 'data' => ['url' => $oVkApiController->getUrl($aPost)] ];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getCurrentVersion($aPost)
    {
        $aData = Setting::where('name', '=', $aPost['options']['name_setting'])->first()->toArray();
        
        if (!empty($aData)) $aResult = ['result' => 1, 'data' => $aData];
        else $aResult = ['result' => 1, 'data' => []];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function getAllItems($aPost)
    {
        if (!empty($aPost['author_id']))
        {
            //продукция
            $aData = Catalog::where('author_id', '=', $aPost['author_id'])
                    ->orderBy('id', 'asc')
                    ->get()
                    ->toArray();
            
            $aResult['catalog'] = $aData;
            $aData = [];
            
            //заказы
            $aData = Order::where('author_id', '=', $aPost['author_id'])
                    ->orderBy('id', 'asc')
                    ->get()
                    ->toArray();
            
            $aResult['orders'] = $aData;
            $aData = [];
            
            //статусы заказов
            $aData = OrderStatus::all()
                    ->toArray();
            
            $aResult['orders_status'] = $aData;
            $aData = [];
            
            //пользователи
            $aData = Client::where('author_id', '=', $aPost['author_id'])
                    ->orderBy('id', 'asc')
                    ->get()
                    ->toArray();
            
            $aResult['clients'] = $aData;
            $aData = [];
            
            //типы клиентов
            $aData = ClientType::all()
                    ->toArray();
            
            $aResult['clients_types'] = $aData;
            $aData = [];
            
            $aResult = ['result' => 1, 'data' => $aResult];
        }
        else $aResult = ['result' => 0];
        
        print json_encode($aResult);
        
        return;
    }
    
    public function dumpData($aPost)
    {
        dd($aPost);
    }
    
}
