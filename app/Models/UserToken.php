<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = 'users_token';
    protected $fillable = ['author_id', 'access_token', 'sreda_key', 'social_network', 'password'];
}
