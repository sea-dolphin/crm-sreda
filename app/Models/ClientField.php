<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientField extends Model
{
    protected $table = 'clients_fields';
    protected $fillable = ['author_id', 'client_id', 'field_group', 'field_name', 'field_value'];
}
