<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderUserStatus extends Model
{
    protected $table = 'orders_user_statuses';
    protected $fillable = ['author_id', 'name', 'icon', 'type_item', 'sorting'];
}
