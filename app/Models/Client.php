<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $fillable = ['author_id', 'client_id', 'parent_id', 'group_id', 'status_id', 'task_id', 'type_id', 'name', 'city', 'phone', 'image', 'comments', 
        'work_phone', 'date_of_birth', 'place_of_work', 'site', 'email', 'skype'];
    
    public function contact_fields()
    {
        return $this->hasMany('App\Models\ClientField', 'author_id', 'author_id');
    }
    
}
