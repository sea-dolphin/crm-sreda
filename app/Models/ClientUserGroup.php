<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientUserGroup extends Model
{
    protected $table = 'clients_user_groups';
    protected $fillable = ['author_id', 'parent_id', 'name', 'icon', 'type_item', 'sorting'];
}
