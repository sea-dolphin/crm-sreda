<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectExecuter extends Model
{
    protected $table = 'project_executers';
    protected $fillable = ['parent_id', 'author_id', 'executer_id'];
}
