<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientStatus extends Model
{
    protected $table = 'clients_status';
    protected $fillable = ['status_name', 'icon'];
}
