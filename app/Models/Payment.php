<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = ['author_id', 'tariff_id', 'invoiceId', 'action_end', 'price', 'status', 'paymentType', 'orderSumAmount', 'message', 'techMessage', 
        'orderSumCurrencyPaycash', 'orderSumBankPaycash', 'requestDatetime', 'shopSumAmount'];
}
