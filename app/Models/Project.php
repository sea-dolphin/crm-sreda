<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    protected $fillable = ['author_id', 'name', 'description', 'parent_id', 'it_group', 'it_checklist', 'status_id'];
    
    public function project_executers()
    {
        return $this->hasMany('App\Models\Project', 'id', 'project_id');
    }
}
