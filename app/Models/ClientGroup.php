<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientGroup extends Model
{
    protected $table = 'clients_groups';
    protected $fillable = ['parent_id', 'name', 'icon', 'sorting', 'type_item'];
    
    
}
