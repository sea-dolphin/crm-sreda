<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['author_id', 'client_id', 'order_id', 'product_id', 'status_id', 'amount', 'price', 'total_price', 'comment', 'delivery_address',
        'budget', 'order_name'];
    
    public function products_list()
    {
        return $this->hasMany('App\Models\Catalog', 'id', 'product_id');
    }
    
    public function product_one()
    {
        return $this->hasOne('App\Models\Catalog', 'id', 'product_id');
    }
    
    public function get_client()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }
    
}
