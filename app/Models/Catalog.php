<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $table = 'catalog';
    protected $fillable = ['author_id', 'name', 'description', 'price', 'it_category', 'category_id', 'balance', 'my_sorting', 'image'];
}
