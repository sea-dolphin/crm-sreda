<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->nullable();
            $table->integer('tariff_id')->nullable();
            $table->integer('invoiceId')->nullable();
            $table->decimal('orderSumAmount', 8, 2)->nullable();
            $table->string('message', 255)->nullable();
            $table->string('techMessage', 255)->nullable();
            $table->decimal('orderSumCurrencyPaycash', 8, 2)->nullable();
            $table->decimal('orderSumBankPaycash', 8, 2)->nullable();
            $table->dateTime('requestDatetime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
