<!DOCTYPE html>
<html lang="ru">
    <head>
            <meta charset="utf-8">
            <title>Оплата подписки для CRM Sreda</title>
            <!--link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500&subset=latin,cyrillic' rel='stylesheet' type='text/css'-->

            <link rel="stylesheet" href="/frontend/css/style0.css">
            <style>body{font-family:Roboto}
                    {*box-sizing:border-box;}
            </style>
    </head>
    <body>
        <div class="page-wrapper">
            <div class="content">
                <div class="title">Выберите тариф</div>
                <ul class="tariff_id">
                        <li class="sel" name="1"><span class="name">{{$oData[0]->name}}</span><span class="price">{{$oData[0]->price}}</span><span class="desc">Доступ на 1 месяц</span></li>
                        <li name="2"><span class="name">{{$oData[1]->name}}</span><span class="price">{{$oData[1]->price}}</span><span class="desc">Доступ на 3 месяца</span></li>
                        <li name="3"><span class="name">{{$oData[2]->name}}</span><span class="price">{{$oData[2]->price}}</span><span class="desc">Доступ на 6 месяцев</span></li>
                        <li name="4"><span class="name">{{$oData[3]->name}}</span><span class="price">{{$oData[3]->price}}</span><span class="desc">Пожизненный доступ</span></li>
                </ul>

                <div class="title">Выберите метод оплаты</div>
                <ul class="paymentType">
                        <li class="sel" name="AC">Банковская карта</li>
                        <!--li name="SB">Сбербанк Онлайн</li-->
						<li name="PC">Яндекс.Деньги</li>
						<!--li name="MC">Баланс телефона</li-->
						<!--li name="WM">WebMoney</li-->
						<li name="AB">Альфа-Клик</li>
						<li name="PB">Интернет-банк Промсвязьбанка</li>
<!--                        <li name="yandexmoney">Инвойсинг</li>-->
                        <li name="QW">QIWI Wallet</li>
                        <li name="QP">Доверительный платеж (Куппи.ру)</li>
<!--                        <li name="yandexmoney">Оплата в терминалах и кассах за пределами России</li>-->
                </ul>
				<input id="submit" type="submit">
                <form id="form" action="https://money.yandex.ru/eshop.xml" method="post">
                        <input name="shopId" value="{{$aConfig['shopId']}}" type="hidden">
                        <input name="scid" value="{{$aConfig['scId']}}" type="hidden">
                        <input name="customerNumber" value="377209368" type="hidden">
                        <input name="sum" value="299" type="hidden">
                        <input name="author_id" value="{{$nAuthorId}}" type="hidden">
                        
                        <input name="paymentType" value="AC" type="hidden">
                        <input name="tariff_id" value="1" type="hidden">
                        <input type="submit">
                </form>
            </div>
        </div>

        <footer>

        </footer>
        <script src="/frontend/js/jquery.min.js"></script>
        <script src="/frontend/js/payment.js"></script>
    </body>
</html>						