<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="/frontend/css/style-tariff.css" rel="stylesheet">
        <title>Тарифы на CRM Sreda</title>
    </head>
    <body>
        <div class="top">
            <div class="wrapper">
                <div class="logo">
                    <img src="http://roob.in/img-host/sreda.svg" width="50" height="50">
                </div>
                <div class="text">
                    Социальная CRM нового поколения.<br>Управлять клиентами просто. Не имеет аналогов.
                </div>
                <div class="menu">
                    <div class="lines-menu"></div>
                    <div class="lines-menu"></div>
                    <div class="lines-menu"></div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="wrapper">
                <div class="text">
                    Простой виджет для браузера, который создаёт из социальной сети ВК полноценную CRM систему. <br>
                    Обязателен для установки всем, кто живёт и работает ВКОНТАКТЕ  <br>
                    Попробуйте, это бесплатно! <br>
                    Для установки перейдите по ссылке http://sreda.one <br>
                    Зачем нужен виджет? <br>
                    - группировать имеющиеся контакты и упорядочить свою деятельность; <br>
                    - назначать бизнес-статусы пользователям, комментировать и вести историю работы с клиентом; <br>
                    - совершать продажи, заключать сделки с высокой скоростью не покидая соц. сети, без дальнейшего их ведения в exel, тетрадке или на стороннем сайте; <br>
                    - оптимизация процессов = гарантированный рост продаж; <br>
                    Идеально подходит для: <br>
                    - IT, фрилансеры <br>
                    - риэлторы <br>
                    - e-commerce (онлайн-магазины) <br>
                    - mlm компании <br>
                    - всем, у кого работа с клиентами в ВК. <br>
                    Попробуйте сервис бесплатно! <br>
                    Для установки перейдите по ссылке <a href="/">http://sreda.one</a> <br>
                    - Без регистрации. <br>
                    - Установка в 2 клика. <br>
                    - Идеальная работа в новом дизайне ВК. <br>
                    - Не отвлекает. Бизнес всегда под рукой. <br>
                    Клиенты, заказы, сделки прямо ВКОНТАКТЕ.<br>

                </div>
                <div class="image">
                    <img src="/frontend/images/im_1.png">
                </div>
                <table>
                    <tr>
                        <td width="20%"><img src="/frontend/images/tariffs/1.png"></td>
                        <td width="20%"><img src="/frontend/images/tariffs/2.png"></td>
                        <td width="20%"><img src="/frontend/images/tariffs/3.png"></td>
                        <td width="20%"><img src="/frontend/images/tariffs/4.png"></td>
                        <td width="20%"><img src="/frontend/images/tariffs/5.png"></td>
                    </tr>
                    <tr>
                        <td class="price">0 руб.</td>
                        <td class="price">299 руб.</td>
                        <td class="price">799 руб.</td>
                        <td class="price">1499 руб.</td>
                        <td class="price">50 000 руб.</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>7 дней полного функционала сервиса бесплатно!</strong><br>
                            Бесплатный тариф:<br>
                            – 1 пользователь<br>
                            – Не более 15 быстрых фраз<br>
                            – Не более 50 клиентов<br>
                            – Не более 20 заказов<br>
                            – Модуль проектной работы недоступен.<br>
                        </td>
                        <td align="left" valign="top">Тариф на одного пользователя при оплате за один месяц. </td>
                        <td align="left" valign="top">При оплате за 3 месяца.</td>
                        <td align="left" valign="top">При оплате за 6 месяцев.</td>
                        <td align="left" valign="top">Лицензия на пожизненный доступ.</td>
                    </tr>
                </table>
                <center>
                    <a href="/payment"><button class="order" style="cursor: pointer;">Заказать</button></a>
                </center>
                <br><br><br>
            </div>
        </div>
    </body>
</html>