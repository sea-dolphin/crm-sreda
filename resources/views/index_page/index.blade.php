<!DOCTYPE html>
<html lang="ru">

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta name="description" content="description">
	<meta name="keywords" content="keywords">
	<meta property="business:contact_data:locality" content="город">
	<meta property="business:contact_data:street_address" content="адрес">
	<meta property="business:contact_data:postal_code" content="почтовый индекс">
	<meta property="business:contact_data:email" content="е-почта">
	<meta property="business:contact_data:phone_number" content="телефон">
	<meta property="business:contact_data:website" content="ссылка на сайт">
	<meta property="og:image" content="изображение для соц. сетей" />
	<meta property="og:title" content="заголовок для соц. сетей" />
	<meta property="og:site_name" content="имя сайта для соц. сетей" />
	<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/khagbihjjpcpnalkhnmkphgbfhncheab">
	<title>Sreda</title>
	<link rel="stylesheet" href="./css/main.css">
	<link rel="stylesheet" href="./css/vendor/modal.css">
</head>

<body>
	<header>
		<div class="limit">
			<div>
				<div class="logo"><img src="http://roob.in/img-host/sreda.svg" width="50" height="50" alt="Sreda CRM"></div>
				<div class="text">Уникальный и простой виджет, который создает из социальной сети полноценную CRM систему для работы с клиентами и сотрудниками</div>
			</div>
			<a href="https://vk.com/sreda_one" onclick="yaCounter40600530.reachGoal('social'); return true;" target="_BLANK" rel="noopener noreferrer" class="btn"><span class="vk-logo"></span>Официальная группа</a>
		</div>
	</header>
	<div class="limit clearfix">
		<div class="left">
			<section>
				<div class="limit">
					<div class="title">Управляйте заказами <span style="white-space: nowrap;"><span class="vk-mark">в</span>контакте</span>
					</div>
					<div class="desc1">SOCIAL CRM SREDA - это клиенты, заказы, сделки.</div>
					<button id="install-extension" class="btn">Установить расширение</button>
					<div class="desc2">Установка в 2 клика. Без регистрации.</div>
				</div>
			</section>
			<footer>
				<div class="limit">
					<div class="text">Новое поколение работает и покупает там, где удобнее!</div>
					<div class="footer-links"><a href="/tariff">Тарифы</a></div>
				</div>
			</footer>
		</div>
		<div class="right">
			<video src="./frontend/video/sreda.mp4" width="80%" poster="./images/video.jpg" preload loop autoplay></video>
		</div>
	</div>
	<div class="modal-wrapper">
		<div data-modal="bad-browser">
			<h1>Ваш браузер не подходит для расширения</h1>
			<p>
				Используйте следующие браузеры:
			</p>
			<ul>
				<li><a href="https://www.google.ru/chrome/browser/desktop/"><img src="http://roob.in/img-host/Google_Chrome.svg" alt="Google Chrome"></a></li>
				<li><a href="https://browser.yandex.ru/desktop/main/"><img src="http://roob.in/img-host/Yandex_Browser.png" alt="Яндекс.Браузер"></a></li>
			</ul>
		</div>
	</div>
	<script type="text/javascript">
	(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=CN6xH*1ZYo*7ETJre9jrlnF2t89Yq7QcIru3OgRICViFU8zo/BZNgM/Rn1GT746khZxYesw*njoa5*rVM4sadtEpkvKP2zhmo8t7pjbmJ2qNxIHM38KW7pqrZW33IlQBE5FTSbjiafCPnYijf7fuom8WqPFxjlvfXZvJyY07CZs-&pixel_id=1000036835';
	</script>
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>
	<!-- VK Widget -->
	<div id="vk_community_messages"></div>
	<script type="text/javascript">
	VK.Widgets.CommunityMessages("vk_community_messages", 130806445, {});
	</script>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	(function(d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter40600530 = new Ya.Metrika({
					id: 40600530,
					clickmap: true,
					trackLinks: true,
					accurateTrackBounce: true,
					webvisor: true
				});
			} catch (e) {}
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function() {
				n.parentNode.insertBefore(s, n);
			};
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else {
			f();
		}
	})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/40600530" style="position:absolute; left:-9999px;" alt="" /></div>
	</noscript>
	<!-- /Yandex.Metrika counter -->
	<script src="./js/vendor/modal.min.js"></script>
	<script>
	modal.init()
	var _ua = window.navigator.userAgent;
	var browser = {
		version: (_ua.match( /.+(?:me|ox|on|rv|it|era|opr|ie)[\/: ]([\d.]+)/ ) || [0,'0'])[1],
		opera: (/opera/i.test(_ua) || /opr/i.test(_ua)),
		msie: (/msie/i.test(_ua) && !/opera/i.test(_ua) || /trident\//i.test(_ua)),
		msie6: (/msie 6/i.test(_ua) && !/opera/i.test(_ua)),
		msie7: (/msie 7/i.test(_ua) && !/opera/i.test(_ua)),
		msie8: (/msie 8/i.test(_ua) && !/opera/i.test(_ua)),
		msie9: (/msie 9/i.test(_ua) && !/opera/i.test(_ua)),
		mozilla: /firefox/i.test(_ua),
		chrome: /chrome/i.test(_ua),
		safari: (!(/chrome/i.test(_ua)) && /webkit|safari|khtml/i.test(_ua)),
		iphone: /iphone/i.test(_ua),
		ipod: /ipod/i.test(_ua),
		iphone4: /iphone.*OS 4/i.test(_ua),
		ipod4: /ipod.*OS 4/i.test(_ua),
		ipad: /ipad/i.test(_ua),
		android: /android/i.test(_ua),
		bada: /bada/i.test(_ua),
		mobile: /iphone|ipod|ipad|opera mini|opera mobi|iemobile|android/i.test(_ua),
		msie_mobile: /iemobile/i.test(_ua),
		safari_mobile: /iphone|ipod|ipad/i.test(_ua),
		opera_mobile: /opera mini|opera mobi/i.test(_ua),
		opera_mini: /opera mini/i.test(_ua),
		mac: /mac/i.test(_ua),
		search_bot: /(yandex|google|stackrambler|aport|slurp|msnbot|bingbot|twitterbot|ia_archiver|facebookexternalhit)/i.test(_ua)
	};

	console.log(browser);

	if (browser.chrome == true && browser.opera == false && browser.mobile == false) {
		function yeah(argument) {
			if (typeof(yaCounter40600530) != "undefined") {yaCounter40600530.reachGoal('action'); console.log("metrika: action")}
			console.log("Fuck Yeah!");
		}
		function onInstalled() {
			button.disabled = true;
			button.textContent = 'Расширение уже установлено =).';
			// In reality, you would show some nice features on the page.
		}
		var button = document.getElementById('install-extension');
		if (document.documentElement.hasAttribute('attribute-set-by-extension')) {
			onInstalled();
		} else {
			button.onclick = function() {
				if (typeof(yaCounter40600530) != "undefined") {yaCounter40600530.reachGoal('click'); console.log("metrika: click")}
				chrome.webstore.install(undefined, function() {
					yeah();
				}, function(error, errorCode) {
					// alert('chrome.webstore.install failed. Error:\n' + error);
				});
			};
		}
	} else {
		if (typeof(yaCounter40600530) != "undefined") {yaCounter40600530.reachGoal('badBrowser'); console.log("metrika: badBrowser")}
		console.log("Смени браузер ;)");
		var button = document.getElementById('install-extension');
		button.onclick = function() {
			modal.open('bad-browser')
		};
	}
	</script>
</body>

</html>
